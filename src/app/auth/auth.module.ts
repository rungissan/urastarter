import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ComponentsModule } from '../shared/components';
import { TranslateModule } from '@ngx-translate/core';
import { AuthSandbox } from './auth.sandbox';
import { AuthApiClient } from './authApiClient.service';
import { ValidationService } from '../shared/utility/validation.service';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule
    ],
  declarations: [RegisterComponent, LoginComponent],
  providers: [AuthApiClient, AuthSandbox, ValidationService]
})
export class AuthModule {}
