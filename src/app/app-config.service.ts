import { Injectable } from '@angular/core';
import { map, catchError } from "rxjs/operators";
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import {
  Headers,
  RequestOptions
}                     from '@angular/http';

@Injectable()
export class ConfigService {

  private config: Object
  private env: Object

  constructor(private http: HttpClient) {}

  /**
   * Loads the environment config file first. Reads the environment variable from the file
   * and based on that loads the appropriate configuration file - development or production
   */
  load() {
    return new Promise((resolve, reject) => {
      let headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'DataType': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      this.http.get('/config/env.json').pipe(
      map((res:any) => res))
      .subscribe((env_data) => {
        this.env = env_data;
        console.log(this.env);

        this.http.get('/config/' + env_data.env + '.json').pipe(
          map((res:any) => res),
          catchError((error: any) => {
            return Observable.throw(error.error || 'Server error');
          }))
          .subscribe((data) => {
            this.config = data;
            console.log(this.config);
            resolve(true);
          });
      });

    });
  }

  /**
   * Returns environment variable based on given key
   *
   * @param key
   */
  getEnv(key: any) {
    return this.env[key];
  }

  /**
   * Returns configuration value based on given key
   *
   * @param key
   */
  get(key: any) {
    return this.config[key];
  }
}