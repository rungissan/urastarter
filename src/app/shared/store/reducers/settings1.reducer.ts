import {
  SettingsActions,
  SettingsActionTypes
} from '../actions/settings1.actions';

export const NIGHT_MODE_THEME = 'BLACK-THEME';

export const initialState: SettingsState = {
  theme: 'DEFAULT-THEME',
  autoNightMode: false,
  pageAnimations: true,
  pageAnimationsDisabled: false,
  elementsAnimations: true
};

export function settingsReducer(
  state: SettingsState = initialState,
  action: SettingsActions
): SettingsState {
  switch (action.type) {
    case SettingsActionTypes.CHANGE_THEME:
      return { ...state, theme: action.payload.theme };

    case SettingsActionTypes.CHANGE_AUTO_NIGHT_AUTO_MODE:
      return { ...state, autoNightMode: action.payload.autoNightMode };

    case SettingsActionTypes.CHANGE_ANIMATIONS_PAGE:
      return { ...state, pageAnimations: action.payload.pageAnimations };

    case SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS:
      return {
        ...state,
        elementsAnimations: action.payload.elementsAnimations
      };

    case SettingsActionTypes.CHANGE_ANIMATIONS_PAGE_DISABLED:
      return {
        ...state,
        pageAnimations: false,
        pageAnimationsDisabled: action.payload.pageAnimationsDisabled
      };

    default:
      return state;
  }
}

export interface SettingsState {
  theme: string;
  autoNightMode: boolean;
  pageAnimations: boolean;
  pageAnimationsDisabled: boolean;
  elementsAnimations: boolean;
}
