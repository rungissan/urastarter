import { Injectable } from '@angular/core';

export type RouteAnimationType = 'ALL' | 'PAGE' | 'ELEMENTS' | 'NONE';

@Injectable()
export class AnimationsService {
  static routeAnimationType: RouteAnimationType = 'NONE';

  static isRouteAnimationsType(type: RouteAnimationType) {
    return AnimationsService.routeAnimationType === type;
  }
  // isRouteAnimationsType: any;
  constructor() {}

  updateRouteAnimationType(
    pageAnimations: boolean,
    elementsAnimations: boolean
  ) {
    AnimationsService.routeAnimationType =
      pageAnimations && elementsAnimations
        ? 'ALL'
        : pageAnimations
          ? 'PAGE'
          : elementsAnimations
            ? 'ELEMENTS'
            : 'NONE';
  }
}
