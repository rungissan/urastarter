import { NgModule, Optional, SkipSelf, APP_INITIALIZER } from '@angular/core';
// import { CommonModule }                        from   '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { storeFreeze } from 'ngrx-store-freeze';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { environment } from '@env/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';

// Store
import { store } from './shared/store';

// Effects
import { AuthEffects } from '../shared/store/effects/auth.effect';
import { ProductsEffects } from '../shared/store/effects/products.effect';

// Guards
import { AuthGuard } from '../shared/guards/auth.guard';
import { CanDeactivateGuard } from '../shared/guards/canDeactivate.guard';

import { initStateFromLocalStorage } from './meta-reducers/init-state-from-local-storage.reducer';
import { LocalStorageService } from './local-storage/local-storage.service';
// import { authReducer } from './auth/auth.reducer';
import { reducers, State } from '../shared/store';
// import { AuthEffects } from './auth/auth.effects';
import { AuthGuardService } from './auth/auth-guard.service';
import { AnimationsService } from './animations/animations.service';
import { AuthApiClient } from '../auth/authApiClient.service';
import { ConfigService } from '../app-config.service';
// import { ProductsEffects }                     from   '../shared/store/effects/products.effect';
// import { debug }                               from   './meta-reducers/debug.reducer';

export const metaReducers: MetaReducer<State>[] = [initStateFromLocalStorage];

if (!environment.production) {
  metaReducers.unshift(storeFreeze);
  if (!environment.test) {
    metaReducers.unshift();
  }
}
// !environment.production ? [storeFreeze] : [];

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
export function configServiceFactory(config: ConfigService) {
  return () => config.load();
}

@NgModule({
  imports: [
    // angular

    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),

    // ngrx
    StoreModule.forRoot(reducers, { metaReducers }),
    // StoreModule.forRoot(
    //   {
    //     auth: authReducer
    //   },
    //   { metaReducers }
    // ),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    EffectsModule.forRoot([AuthEffects])
  ],
  declarations: [],
  providers: [
    LocalStorageService,
    AuthGuardService,
    AnimationsService,
    AuthApiClient,
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true
    }
  ]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
