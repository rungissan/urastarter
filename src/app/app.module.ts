import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, PLATFORM_ID, APP_ID, Inject } from '@angular/core';

import { isPlatformBrowser} from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';

import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';

import { SettingsModule } from './settings';
import { StaticModule } from './static';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { AppSandbox } from './app.sandbox';

@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'ura-super-start' }),
    // core & shared
    CoreModule,
    SharedModule,

    // features
    StaticModule,
    SettingsModule,

    // app
    AppRoutingModule,
    TransferHttpCacheModule,

    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  declarations: [AppComponent],
  providers: [AppSandbox],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor( @Inject(PLATFORM_ID) private platformId: Object, @Inject(APP_ID) private appId: string ) {
    const platform = isPlatformBrowser(this.platformId) ?
    'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${this.appId}`);
  }
}
